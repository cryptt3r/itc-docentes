//
//  ViewController.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 09/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import "ViewController.h"
#import "MateriasViewController.h"
//#import "Docente.h"


@interface ViewController ()

@end

@implementation ViewController
@synthesize txtUsuario, txtClave, docente;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnLogin:(id)sender {
    NSString *usuario = [txtUsuario text];
    NSString *clave = [txtClave text];
    
    NSLog(@"Usuario: %@ Clave: %@",usuario,clave);
    docente = [[Docente alloc]initWhitLogin:[txtUsuario text] clave:[txtClave text]];
    // Docente *docente = [[Docente alloc]initWhitLogin:@"920" clave:@"0731"];
    if (docente != nil) {
        NSLog(@"pass %@ ", [docente getHash]);
        [docente getMateriasServer];
        [self performSegueWithIdentifier:@"successLogin" sender:self];
    }
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"successLogin"]) {
        MateriasViewController *materiasView = [segue destinationViewController];
        materiasView.materias = docente.materias;
    }
}
@end
