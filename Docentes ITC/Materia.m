//
//  Materia.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import "Materia.h"

@implementation Materia

- (NSString *)clave {
    return clave;
}

- (void)setClave:(NSString *)newValue {
    clave = newValue;
}

- (NSString *)nombre {
    return nombre;
}

- (void)setNombre:(NSString *)newValue {
    nombre = newValue;
}

- (NSString *)horario {
    return horario;
}

- (void)setHorario:(NSString *)newValue {
    horario = newValue;
}

- (NSMutableArray *)temas {
    return temas;
}

- (void)setTemas:(NSMutableArray *)newValue {
    temas = newValue;
}

@end
