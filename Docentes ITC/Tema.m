//
//  Tema.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import "Tema.h"

@implementation Tema



- (NSString *)tema {
    return tema;
}

- (void)setTema:(NSString *)newValue {
    tema = newValue;
}

- (NSString *)programadoInicio {
    return programadoInicio;
}

- (void)setProgramadoInicio:(NSString *)newValue {
    programadoInicio = newValue;
}

- (NSString *)programadoTermino {
    return programadoTermino;
}

- (void)setProgramadoTermino:(NSString *)newValue {
    programadoTermino = newValue;
}

- (NSString *)realInicio {
    return realInicio;
}

- (void)setRealInicio:(NSString *)newValue {
    realInicio = newValue;
}

- (NSString *)realTermino {
    return realTermino;
}

- (void)setRealTermino:(NSString *)newValue {
    realTermino = newValue;
}

- (NSString *)evaluacionProgramada {
    return evaluacionProgramada;
}

- (void)setEvaluacionProgramada:(NSString *)newValue {
    evaluacionProgramada = newValue;
}

- (NSString *)evaluacionReal {
    return evaluacionReal;
}

- (void)setEvaluacionReal:(NSString *)newValue {
    evaluacionReal = newValue;
}

@end
