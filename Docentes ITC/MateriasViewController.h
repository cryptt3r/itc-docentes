//
//  MateriasViewController.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 12/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MateriasViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property NSMutableArray *materias;

@property (weak, nonatomic) IBOutlet UITableView *tablaMaterias;


@end
