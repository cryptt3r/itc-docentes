//
//  Materia.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Materia : NSObject
{
    NSString *clave;
    NSString *nombre;
    NSString *horario;
    NSMutableArray *temas;
}

- (NSString *)clave;

- (void)setClave:(NSString *)newValue;

- (NSString *)nombre;

- (void)setNombre:(NSString *)newValue;

- (NSString *)horario;

- (void)setHorario:(NSString *)newValue;

- (NSMutableArray *)temas;

- (void)setTemas:(NSMutableArray *)newValue;

@end
