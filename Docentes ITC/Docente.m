//
//  Docente.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import "Docente.h"
#import "Server.h"
#import "Materia.h"

@implementation Docente


-(Docente *)initWhitLogin:(NSString *)user clave:(NSString *)clave
{
    if (self = [super init])
    {
        hash = [[[Server alloc]init]login:user clave:clave];
        if ([hash length]>0) {
            [self setUsuario: user];
            [self setPass: clave];
            return self;
        }
        else {
            return nil;
        }
    }
    return nil;
}
-(NSString *)getHash {
    return hash;
}
-(void)setHash:(NSString *)newHash {
    self.hash = newHash;
}
- (NSString *)usuario {
    return usuario;
}

- (void)setUsuario:(NSString *)newValue {
    usuario = newValue;
}

- (NSString *)pass {
    return pass;
}

- (void)setPass:(NSString *)newValue {
    pass = newValue;
}

- (NSMutableArray *)materias {
    return materias;
}

- (void)setMaterias:(NSMutableArray *)newValue {
    materias = newValue;
}


-(void)getMateriasServer {
    Server *servidor = [[Server alloc]init];
    materias = [servidor getMaterias:usuario hash:hash];
    for (Materia *mat in materias) {
        [mat setTemas:[servidor getTemas:[mat clave] usuario:usuario hash:hash]];
        NSLog(@"Materia: %@ Tiene: %i temas.", [mat nombre], [[mat temas]count]);
    }
}


@end
