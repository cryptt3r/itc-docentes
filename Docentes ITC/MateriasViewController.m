//
//  MateriasViewController.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 12/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//
#import "Materia.h"
#import "MateriasViewController.h"

@interface MateriasViewController ()

@end

@implementation MateriasViewController
@synthesize materias;
@synthesize tablaMaterias;
- (void)viewDidLoad {
    [super viewDidLoad];
    for (Materia *mat in materias) {
        NSLog(@"Materia desde Segunda vista: %@",[mat nombre]);
    }
    self.tablaMaterias.delegate = self;
    self.tablaMaterias.dataSource = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return materias.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"celdaMateria";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    UILabel *lblNombreMateria = (UILabel *)[cell viewWithTag:1];
    UILabel *lblHorarioMateria = (UILabel *)[cell viewWithTag:2];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Materia *mat = [materias objectAtIndex:indexPath.row];
    lblNombreMateria.text = [mat nombre];
    lblHorarioMateria.text = [mat horario];
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}

//#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
