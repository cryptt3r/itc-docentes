//
//  Docente.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Docente : NSObject {
    NSString *usuario;
    NSString *pass;
    NSString *hash;
    NSMutableArray *materias;
}


-(Docente *)initWhitLogin: (NSString *)user clave:(NSString *)clave;
-(NSString *)getHash;
-(void) setHash:(NSString *) newHash;

- (NSString *)usuario;

- (void)setUsuario:(NSString *)newValue;

- (NSString *)pass;

- (void)setPass:(NSString *)newValue;

- (NSMutableArray *)materias;

- (void)setMaterias:(NSMutableArray *)newValue;


-(void)getMateriasServer;


@end
