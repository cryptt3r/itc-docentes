//
//  Server.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//
#import "TFHpple.h"
#import "Server.h"
#import "Materia.h"
#import "Tema.h"
@implementation Server


- (id) init {
    if( (self=[super init])) {
        URLBase = @"http://intertec.itculiacan.edu.mx/cgi-bin/sie.pl";
        Request =
        [
         NSMutableURLRequest requestWithURL: [NSURL URLWithString: URLBase]
         cachePolicy:NSURLRequestUseProtocolCachePolicy
         timeoutInterval:60.0
         ];
        [Request setHTTPMethod:@"POST"];
        
    }
    return self;
}


- (NSString *)login:(NSString *)usuario clave:(NSString *)clave {
    
    NSString *password;
    
    
    NSString *post = [NSString stringWithFormat:@"Opc=MAINDOCENTE&Control=%@&password=%@", usuario,clave];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    [Request setHTTPBody:postData];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    
    NSInteger cnt =0;
    while (cnt<5) {
        NSData * data = [NSURLConnection sendSynchronousRequest:Request
                                     returningResponse:&response
                                                 error:&error];
        if (error == nil)
        {
            
            TFHpple *frameParser = [TFHpple hppleWithHTMLData:data];
            
            NSArray *framesNodes = [frameParser searchWithXPathQuery:@"//frame[@name='principal']"];
            
            for (TFHppleElement *element in framesNodes) {
                NSString *strSRC =[element objectForKey:@"src"];
                
                NSInteger pos1 = [strSRC rangeOfString:@"&Password="].location + 10;
                NSInteger pos2 = [strSRC rangeOfString:@"&dummy="].location - pos1;
                
                if (pos1 > 0  && pos2 > 0){
                    
                    NSRange rango =NSMakeRange(pos1, pos2);
                    password = [strSRC substringWithRange: rango];
                }
            }
            
        }
        
        if ([password length]>0) {
            break;
        }
        
        cnt++;
    }
    
    return password;
}


- (NSMutableArray *)getMaterias:(NSString *)usuario hash:(NSString *)hash {
    ///cgi-bin/sie.pl?Opc=HORARIODOC&Control=920&Password=1D130CF51F7E20BB26DF426C98B2F3FF&Param=PRDO&dummy=0'
    NSString *get = [NSString stringWithFormat:@"?Opc=HORARIODOC&Control=%@&Password=%@&Param=PRDO&dummy=0", usuario,hash];
    NSString *urlGET = [URLBase stringByAppendingString:get];
  
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlGET]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&error];

    NSMutableArray *materiasArray=[[NSMutableArray alloc] initWithArray:nil];
    if (error == nil)
    {
        //NSString *responseText = [[NSString alloc] initWithData:data encoding: NSASCIIStringEncoding];
        //NSLog(@"Response: %@", responseText);

        
        TFHpple *tablesParser = [TFHpple hppleWithHTMLData:data];
        
        NSArray *tablesNodes = [tablesParser searchWithXPathQuery:@"//table"];
        //NSLog(@"Count: %i",[tablesNodes count]);
        NSInteger cnt =0;
        
        // Tabla de materias (Grupos)
        TFHppleElement *tablaGrupos=[tablesNodes objectAtIndex:1];
        
        for (TFHppleElement *hijoTR in [tablaGrupos children]) {
            if (cnt>=3) {
                if([[hijoTR children]count]>0){
                    //TFHppleElement *TFMateriaClv = [[[[hijoTR children]objectAtIndex:0]firstChild]firstChild];
                    TFHppleElement *TFMateriaNom = [[[[hijoTR children]objectAtIndex:1]firstChild]firstChild];
                    TFHppleElement *TFMateriaHor = [[[hijoTR children]objectAtIndex:4]firstChild];
                    if (TFMateriaNom != nil) {
                        NSString *strOnCLick =[TFMateriaNom objectForKey:@"onclick"];
                        
                        NSInteger pos1 = [strOnCLick rangeOfString:@"&MATERIA="].location + 9;
                        NSInteger pos2 = [strOnCLick rangeOfString:@"&dummy="].location - pos1;
                        NSString *strTema;
                        
                        if (pos1 > 0  && pos2 > 0){
                            
                            NSRange rango =NSMakeRange(pos1, pos2);
                            strTema = [strOnCLick substringWithRange: rango];
                        }
                        
                        
                        
                        //NSString *strTema = [[TFMateriaClv text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSString *strMateriaNom = [[TFMateriaNom text]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        NSString *strMateriaHor = [[TFMateriaHor text]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        Materia *mat = [[Materia alloc]init];
                        [mat setClave: strTema];
                        [mat setNombre:strMateriaNom];
                        [mat setHorario: strMateriaHor];
                        [materiasArray addObject:mat];
                       // NSLog(@"Tema: %@",strTema);
                       
                    }
                    
                }
            }
            cnt++;
        }
        
    }
    else {
        
        NSLog(@"Algo salio mal en  getMaterias server");
    }
    
    return  materiasArray;

    
    
}


- (NSMutableArray *)getTemas:(NSString *)clvMateria usuario:(NSString*)usuario hash:(NSString*)hash {
    
    NSString *get = [NSString stringWithFormat:@"?Opc=TEMARIODOC&Control=%@&Password=%@&MATERIA=%@&dummy=0", usuario,hash,clvMateria];
  
    
    NSString *urlGET = [URLBase stringByAppendingString:get];
    
    
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[urlGET stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&error];
    
    NSMutableArray *TemasArray=[[NSMutableArray alloc] initWithArray:nil];
    if (error == nil)
    {
        
        TFHpple *tablesParser = [TFHpple hppleWithHTMLData:data];
        
        NSArray *tablesNodes = [tablesParser searchWithXPathQuery:@"//table"];
       
        // Tabla de Temas
        TFHppleElement *tablaTemas=[tablesNodes objectAtIndex:1];
      
        
        NSInteger cnt =0;
        for (TFHppleElement *hijoTR in [tablaTemas children]) {
            if(cnt>=5 && cnt<[[tablaTemas children]count]-1){
                NSString *strTema                   =[[[[hijoTR firstChild]firstChild]firstChild]text];
               
                NSString *strInicioProgramado      = [[[[hijoTR children]objectAtIndex:2]firstChild]text];
                NSString *strFinProgramado         = [[[[hijoTR children]objectAtIndex:3]firstChild]text];
               
                NSString *strInicioReal            = [[[[hijoTR children]objectAtIndex:4]firstChild]text];
                NSString *strFinReal               = [[[[hijoTR children]objectAtIndex:5]firstChild]text];
               
                NSString *strEvaluacionProgramado  = [[[[hijoTR children]objectAtIndex:6]firstChild]text];
                NSString *strEvaluacionReal        = [[[[hijoTR children]objectAtIndex:7]firstChild]text];
                
                Tema *tema = [[Tema alloc]init];
                
                [tema setTema:strTema];
                [tema setProgramadoInicio:[Server formateaFecha: strInicioProgramado]];
                [tema setRealInicio:[Server formateaFecha: strInicioReal]];
                [tema setProgramadoTermino:[Server formateaFecha: strFinProgramado]];
                [tema setRealTermino:[Server formateaFecha: strFinReal]];
                [tema setEvaluacionProgramada:[Server formateaFecha: strEvaluacionProgramado]];
                [tema setEvaluacionReal:[Server formateaFecha: strEvaluacionReal]];
                
                [TemasArray addObject:tema];
            }
            
            cnt++;
        }
        
    }
    else {
        
        NSLog(@"Algo salio mal en  getMaterias server");
    }
    
    return TemasArray;

}


+ (NSString *)formateaFecha:(NSString *)fecha {
    
    NSString *anio = [fecha substringWithRange: NSMakeRange(0, 2)];
    NSString * mes = [fecha substringWithRange: NSMakeRange(2, 2)];
    NSString * dia = [fecha substringWithRange: NSMakeRange(4, 2)];
    
    return [[[[dia stringByAppendingString:@"/"] stringByAppendingString:mes]stringByAppendingString:@"/"]stringByAppendingString:anio];
    
}


@end
