//
//  ViewController.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 09/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Docente.h"

@interface ViewController : UIViewController


- (IBAction)btnLogin:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtUsuario;
@property (strong, nonatomic) IBOutlet UITextField *txtClave;
@property (nonatomic, strong) Docente *docente;
@end

