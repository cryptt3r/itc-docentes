//
//  AppDelegate.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 09/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

