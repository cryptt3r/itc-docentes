//
//  main.m
//  Docentes ITC
//
//  Created by Joel Jacquez on 09/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
