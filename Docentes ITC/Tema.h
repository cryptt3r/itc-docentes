//
//  Tema.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tema : NSObject{
    NSString *tema;
    NSString *programadoInicio;
    NSString *programadoTermino;
    NSString *realInicio;
    NSString *realTermino;
    NSString *evaluacionProgramada;
    NSString *evaluacionReal;
}

- (NSString *)tema;

- (void)setTema:(NSString *)newValue;


- (NSString *)programadoInicio;

- (void)setProgramadoInicio:(NSString *)newValue;

- (NSString *)programadoTermino;

- (void)setProgramadoTermino:(NSString *)newValue;

- (NSString *)realInicio;

- (void)setRealInicio:(NSString *)newValue;

- (NSString *)realTermino;

- (void)setRealTermino:(NSString *)newValue;

- (NSString *)evaluacionProgramada;

- (void)setEvaluacionProgramada:(NSString *)newValue;

- (NSString *)evaluacionReal;

- (void)setEvaluacionReal:(NSString *)newValue;

@end
