//
//  Server.h
//  Docentes ITC
//
//  Created by Joel Jacquez on 11/12/14.
//  Copyright (c) 2014 Joel Jacquez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Server : NSObject
{
    NSString  *URLBase;
    NSMutableURLRequest *Request;    
}

- (NSString *)login: (NSString *) usuario clave: (NSString *) clave;
- (NSMutableArray *) getMaterias: (NSString *) usuario hash:(NSString *) hash;
- (NSMutableArray *)getTemas:(NSString *)clvMateria usuario:(NSString*)usuario hash:(NSString*)hash;
+ (NSString *)formateaFecha:(NSString *)fecha;
@end
